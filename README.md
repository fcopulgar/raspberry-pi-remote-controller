# Raspberry Pi Remote Controller

## pi-alarm

Programa que desde la Raspberry Pi escucha la cola [MQTT](http://mqtt.org) y ejecuta las instrucciones (prender o apagar los ports).

* port1 => GPIO 11
* port2 => GPIO 13
* port3 => GPIO 15

### Instalación pi-alarm

La primera vez se debe ejecutar:

	npm install

Al iniciar la Raspberry Pi se debe ejecutar:

	./run_pi-alarm.sh

* Se puede hacer que el script inicie con el sistema para evitar ejecutarlo cada vez que se prenda la Raspberry Pi.


## pi-alarm-server

Aplicación web que recibe por GET la instrucción a ejecutar (la manda a la cola MQTT).

### Instalación pi-alarm-server

	sudo npm install

### Correr pi-alarm-server

	node pi-alarm-server.js

### Modo de uso pi-alarm-server

Prender port1 => GPIO 11:

	http://127.0.0.1:3000/alarm/port1/1

Apagar port2 => GPIO 13:

	http://127.0.0.1:3000/alarm/port2/0

### Imágen pi-alarm-server

![pi-alarm-server](https://bytebucket.org/fcopulgar/raspberry-pi-remote-controller/raw/6a2009a703e3f4cbcfc179c3099ceca1260c827f/imagenes/server.png "pi-alarm-server")

## Circuito Raspberry Pi

![Circuito Raspberry Pi](https://bytebucket.org/fcopulgar/raspberry-pi-remote-controller/raw/6a2009a703e3f4cbcfc179c3099ceca1260c827f/imagenes/circuito.png "Circuito Raspberry Pi")

## Video Demostrativo

[![Video Demostrativo](http://img.youtube.com/vi/t8DOyIzKzoI/0.jpg)](https://www.youtube.com/watch?v=t8DOyIzKzoI)