#!/bin/bash

export PATH=$PATH:/usr/local/bin

# cd /home/timmy/pi-alarm
forever start -o process.out.log -e process.err.log -l pi-alarm_forever.log -a pi-alarm.js pi-alarm-config.json
