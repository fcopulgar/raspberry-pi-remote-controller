var mqtt = require('mqtt');
var gpio = require('pi-gpio');
var fs = require('fs');

var currentState = {};
var turnoffTask = null;

var config = {
  mqttClientOpts: {},
  mqttPort: 1883,
  mqttHost: 'localhost',
  mqttCommandsTopic: '/alarm/commands',
  mqttStateTopic: '/alarm/state',
  turnOffDelay: 10000
};

var ports = {
  port1 : 11,
  port2 : 13,
  port3 : 15,
}

// Read configfile
if (process.argv.length > 2) {
  console.log('Parsing config file: '+process.argv[2]);
  var data = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));
  for (var key in data) {
    config[key] = data[key];
  }
}

// Add will to mqttClientOpts for offline reporting
config['mqttClientOpts']['will'] = {
  topic: config['mqttStateTopic'],
  payload: JSON.stringify({currentState: 'offline'}),
  qos: 1,
  retain: true
};

var mqttClient = mqtt.createClient(config['mqttPort'], config['mqttHost'], config['mqttClientOpts']);

var actuator = function(data) {
  if(data.port !== undefined && data.state !== undefined ){
    var port = data.port;
    var state = parseInt(data.state);
    console.log(ports[port]);
    if (ports[port] !== undefined){
      console.log('Switching relay: '+ports[port]+', state:'+state);
      
      gpio.open(ports[port], "output", function(err) {
         gpio.write(ports[port], state, function(err) {
           //updateState(ports[port], data.state);
           gpio.close(ports[port]);
         });
       });
    }
  } else {
    return;
  }

  /*
  if (data.value) {
    // Schedule turnoffTask, cancelling the previous task if exists
    if (turnoffTask != null) {
      clearTimeout(turnoffTask);
    }
    turnoffTask = setTimeout(actuator, data.timeout || config['turnOffDelay'], {value: false});
  } else {
    // If turning off, cancel the outstanding turnoffTask
    if (turnoffTask != null) {
      clearTimeout(turnoffTask);
    }
    turnoffTask = null;
  }*/
};

mqttClient
  .subscribe(config['mqttCommandsTopic'])
  .on('message', function(topic, message) {
    console.log(topic);
    console.log(message);
    if (topic == config['mqttCommandsTopic']) {
      data = JSON.parse(message);
      actuator(data);
    }
  });
